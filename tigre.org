#+TITLE: Tigre
#+AUTHOR: David Pineda
#+LANGUAGE: es
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+OPTIONS: toc:nil   

Repentinamente abrí mis ojos y miré al techo. No sé que habría soñado porque
inmediatamente lo olvidé por completo. Apenas entraba algo de luz, había una
sombra tenue y el ruido de los animales despertando entraba con claridad. Estaba
solo en mi habitación, respirando lentamente, tratando de ordenar las ideas,
tratando de entender que ya estaba despierto. Moví la vista para el lado en que
había dejado el libro que estaba leyendo esos días, marcado en la página que
había quedado, era el que más me gustaba por lo que ya me lo sabía de memoria,
cada una de sus páginas, cada uno de sus dibujos, me gustaba. Si, ya tenía claro
lo que debería hacer, aspiré suficiente aire para gritar.

-- ¡Tengooooooo hambreeeeeee!¡Tengo hambreeeeeee!¡Mamáaaaa!

Nada, nada. Ninguna respuesta. Esto no podía estar pasando. Siempre respondía a
mis llamadas y ahora, por respuesta, solo el eco seco de mi voz. ¿Qué tendría
que hacer? ¿Esperar? ¿Levantarme? ¿Preocuparme? Solo tenía hambre y necesitaba
comer algo, mi desayuno, llenar mi barriga con algo caliente antes de comenzar el
día. No sabría decir cómo me sentiría si mi día no empieza con una buena comida
entre mis tripas. Me desespero, no puedo esperar más. Llamo de nuevo.

-- ¡Tengo haaaaaaambre!¡Tengo hambreeeeeeeee!

Definitivo, debería mover mi cuerpo. Bajar de la cama y caminar torpemente
hasta la cocina. Tomar algo de comer, una fruta, una galleta, y calmarme. Pero
no podía, no pasaba por mi mente que.... No, apenas tenía ganas de valerme por
mí mismo ese día. Estaba además acostumbrado a que mi mamá acudiera siempre que
la necesitaba. ¿Qué podía hacer solo? ¡Solo! No, tenía que levantarme y comer.
Tenía todo el día por delante y muchas cosas por hacer, no podía quedarme
estancado entre las sábanas... como el abuelo. No, no era tan viejo, al
contrario había iniciado la vida y no quería que me trataran así. Quería sol,
aire, patio, jugar con mi perro.

Me giré y me senté en la cama. Dije para mis adentros "¡uno, dos, tres!" y me
paré. No podía dejar de pensar en el mi libro favorito así que lo tomé y lo puse
debajo del brazo, lo volvería a leer mientras tomaba desayuno. Tal vez mamá no
me había escuchado y seguro, ¡seguro!, que estaba ahí preparandome ya lo que más
me gustaba de tomar, una leche con plátano y sandwish de queso con tomate.

Caminé los veinte pasos. ¡Ay! pero si tenía ganas de hacer pis. Me devolví cinco
pasos, dejé el libro sobre el taburete y entré, aún casi dormido logré levantar
la tapa del w.c. y me dispuse a orinar. Poco a poco ya fui despertando. Me lavé
las manos, me mojé un poco la cara y por fín pude abrir los ojos, se me despejó
la mente. Salí como nuevo corriendo, en dos segundos estuve en la cocina
esperando verla ahí, sonriente como todos los días, entregandome todo el amor
que puede dar una madre a su hijo. 

Todo vacío, nada. La cocina estaba intacta, limpia, con las frutas en el centro
de la mesa, las galletas en la alacena. Sabía donde las guardaba mi madre, pero
ella no sabía que yo sabía. Decepción. Nadie en el mundo, en el universo, podría
responderme ahora la sensación que tuve en ese momento, desolación tal vez,
ausencia, vacio. Solo atiné a tomar una naranja y un plátano, tomar un yogurt
del refrigerador, caminar a recuperar mi libro que había dejado olvidado y salir al patio a tomar algo de
sol. Esperaré, me decía, seguro que si esperaba iría a aparecer en algún
momento.

Si me ponía a enumerar los posibles situaciones bajo las cuales mi madre podría
no haber aparecido. Hasta hoy no había pasado un día en que no fuera el primer
ser vivo que podían ver mis ojos al despertar. Hasta la noche anterior también
era el último ser vivo que podía ver, escuchar, sentir antes de dormir. Me había
fallado, no conocía la desesperación, pero si desde este futuro se me da la
oportunidad de describir lo que estaba sintiendo, era eso precisamente,
desesperación. Una creciente sensación de que las cosas no estaban en mis
dominios. 

Del camino de mi cuarto a la cocina estaba la habitación de mi madre, cuando
pase pude ver de soslayo que no había nadie en ella. La noche anterior habiamos
visto pokemón  juntos, me gustaba pokemón y le había pedido que me regalara para
mi cumpleaños una nintendo para poder jugar con mis compañeros de colegio. Ella
trabaja mucho, solo somos los dos en casa y Lolo, un viejo perro que apenas
se mueve pero que le encanta que le acaricie la panza. Si, era imposible,
inaudito, no poder encontrarme con el amor de mi vida, mi corta vida. No estaba,
simplemente se había esfumado.

¿Qué habrá pasado? ¿Qué habrá pasado? ¿Dónde estará mamá? ¿Con quién? ¿Por qué
no está? ¡Ay! No sabía que responder. Los pájaros cantan sus melodías como
siempre, el sol se asoma entre las ramas de los árboles, el aire fresco y los
aromas de las flores llegaban a mi nariz y Lolo cerca, pidiendo que le
acariciaba. ¿Qué podía hacer? Necesitaba pensar, decidir, no podía quedarme ahí.
Antes sí debía comer, alimentarme.

Recordé que debía ver algo más de mi libro. Me lo regalaron para mi cumpleaños,
la primera vez me lo leyó el abuelo, pero pronto se cansó de mis preguntas, eran
tantas que muchas no parecía saberlas. Me lo llevé al patio para hojearlo y pasó
el tiempo. Olvidé por completo que era mi propia fiesta de cumpleaños. Sumerjido
en la lectura de algo que aún no podía descifrar, me concentré en la abstracción
de ls ilustraciones que aparecían cada nueva páginas lograba hacerme la idea de
lo que sucedía en la historia.

Un pequeño y recien nacido tigre empieza a descubrir el mundo, se trepa sobre
los árboles. A veces le gustaba nadar. Poco a poco se va alejando de de la
vigilancia de su madre hasta que logra andar solo por la selva. Se encuentra con
un cazador, esta a punto de morir, lo ve, lo siente. Pero justo en el momento su
madre se interpone y recibe la bala. Mientras muere y gimiendo de dolor la
indica a su hijo que huya, espantado corre y corre hasta perderse en lo más
recóndito de la selva. Se hace la noche y debe vivir. Crece en el corazón de la
selva, aprende a cazar y leer con sus sentidos los signos que le entrega. El
viento, la lluvia, la noche. Se transforma en uno con ella. Pero no olvida lo
sucedido, es una deuda de sangre que debe ser pagada y se encargará de llevarla
a cabo. Se atreve a acercarse a los humanos, husmea, rastrea al cazador. Luego
de tantos años la fuerza está de su lado...

Si, por mi madre lo daría todo como Tigre. Si algo le pasara, ¡si algo le
pasara! Viviré para vengarla. Pero ¿contra quién? ¿Quién caza a los humanos?
Cuando le pregunté a mi abuelo. Él me dijo que solo los humanos serían capaces
de cazar humanos. Que hay humanos muy muy malos, que han hecho atrocidades
indescriptibles para un niño como yo. Sí, me quedé pensando, me gustaban mucho
esas ilustraciones, tan coloridas, a veces transparentes como manchones en
movimientos. Un poquito más allá entre las sombras divisé unos ojos que me
miraban.

Ya había terminado de consumir el yogurt y la naranja. Estaba abriendo el
plátano, inundándome con un aroma suave y delicioso. Me paré, caminé unos
cuantos pasos hacia aquellos ojos, hubo un destello amarillo y desaparecieron en
la arboleda. Decidí seguirlos, se esuchaba el ruido de unos pequeños pasos que
crujían sobre las hojas.  

A partir de ahí no tenía permitido avanzar. Mi madré, que siempre vigilaba mis
pasos, me advertía cada vez que me dirigía más allá de la arboleda. Era
peligroso. Si, era alto el camino sinuoso. Había una quebrada a la que era
difícil de llegar. Pero ahora ella no estaba, no podía impedirme continuar y
satisfacer mi curiosisdad. ¡Aquellos ojos! ¡Qué misterio!

A medida que avanzaba por el sendero los árboles se tornaban más altos y más
densos. Se escuchaba a lo lejos el borbotear del arrollo, seguramente debía de
llegar hasta ahí, no se por qué, pero mi corazón me guiaba a través del sonido
que se dejaba caer sobre mis oídos, la magnificiencia íntima de la naturaleza
por la que me estaba inmiscuyendo.

Llegué al borde del arrollo. Ya había dejado de hacer frio. Estaba poniendose
cálido y sobre una roca se podía estar tranquilo. Seguro que cualquier anciano
se habría asustado de verme ahí, solo. Pero ya sabía nadar y un arrollo así no
podía hacer nada contra mis fuertes brazos y piernas. Pequeño, podía sobrevivir
a un chorro de agua. Me quedé mirando pasar el agua.

Sientes un ruidito reberberante, magnético, que captura con intensidad el
espíritu. Que pasa allí con un chorro que golpea una roca, agua que cae veinte
centímetros y plash, piedritas que se golpean unas con otras. El reflejo de
espejos acúaticos que me llegaban a mi cara la iluminaban. El sentido del tiempo
perdido en una hipnosis recurrente de objetos eternos.

Los ojos se posaron en mí nuevamente, al otro lado del arroyuelo la figura se
tornaba poco a poco distinguible entre las rocas, su pelaje le otorgaba un
camuflaje perfecto, que apenas permitía distinguir sus ojos inundado de
tonalidades amarillas y pardas sobre rayos que partían desde un centro oscuro y
profundo. Esos ojos me miraban y poco a poco olvidaba el hipnotizante ruido para
concentrar mi mirada es su mirada, como una pequeña eternidad, nos miramos uno
al otro sin decir nada por un buen tiempo.

Desapareció cualquier otro sonido excepto la tensión que nuestras dos presencias
provaba en el universo. ¿Quíen era ese animal? ¿Por qué tanto misterio? ¿Por qué
buscaba mirarme y soportaba mi mirada de vuelta? Bajé al arrolluelo y me fui
atravesándo lentamente entre las piedras, el agua mojaba hasta un poco más
arriba de las rodillas.

Había llegado cerca, muy cerca del animal, cuando arrancó corriendo nuevamente
por la orilla unos quince pasos. Nuevamente me acerqué, un poquito más, pero lo
veía dudar de mis intenciones así que volvió a correr. Intenté nuevamente y ya
pude ganarme su confianza. Me permitió acariciarle la cabeza, lentamente, luego
de un rato saltó encima de mi pecho. Clavó impúnemente sus garras sobre mi
camiseta, las atravesó me dolió tanto que casi grité pero no lo hice para no
asustar a la pobre criatura.

-- ¡Eh! Lindo, ¿cómo te llamas?

-- Miauuuuuuu

-- ¿Tienes hambre?

-- Miauuuuuuu

Vamos, seguro estás perdido y asustado, te llevare a mi casa, te daré algo de
comer para que te sientas bien. Lo metí dentro de la camiseta para que fuera
seguro y abrigado. Sentía la bola de pelo en mi pecho, quieto, cálido,
tranquilo. Caminé de vuelta a casa y me encontré con mi madre desesperada. Le
mostre a Tigre y le dije que lo quería, no me dijo nada.

Ahora solo sé que desde entonces me acompañó, a veces me traía algún pájaro
hasta las puertas de mi casa, cazaba con ahínco los roedores y en invierno se
sumergía en el letargo del calor de la estufa. Algo más de diez años que estuvo
Tigre conmigo, estaba viejito, no podía más. Solo me miró con sus mismos ojos
poco antes de morir, despidiendose, y expiró.


