			    ━━━━━━━━━━━━━━━
			     BUEN SERVICIO

			      David Pineda
			    ━━━━━━━━━━━━━━━


Table of Contents
─────────────────




El hombre dobló la esquina y se adentró por un pequeño pasaje cubierto
de adoquines, algunos arces apenas crecidos le daban algo de vida al
lugar, un par de faroles ya comenzaban a encender en esa fría tarde. Se
sentó, sin quitarse su abrigo, en un agradable café a esperar a su cita.

– Un expreso doble, por favor, y galletas –

– ¡Sí, señor! – dijo la señorita

Tomó el periódico, pero no se pudo concentrar en las anodinas noticias
que aparecían en este, tenía algo importante que decir. Su vida pronto
daría un cambio total y en su mente flotaban las ideas y recuerdos. Ya
no estaba joven, pero se veía fuerte y saludable.

Se abre la puerta, entra una mujer madura, vestida con un traje de
oficina, en tonos claros y en su cabello al natural, aparecían algunas
canas que le otorgaban la apariencia de una persona tranquila, que sabe
lo que hace y lo que quiere.

– ¡Perdona! Me atrapó el trabajo.

– ¡Está bien! ¿Cómo estás Lucía? Disfrutaba de este rico café. ¿Te
  sirves uno?

– Un capuchino.

– Dale, lo pediré. ¡Señorita, un capuchino!

– ¿Algo de comer? – llega la chica que atendía.

– Mmmm, quisiera algo muy dulce, ¿tiene torta de tres leches?

– Si, le traigo.

– Hace años que no nos encontrábamos.

– Como cuatro, y apenas nos saludamos en esa fiesta.

– Ibas con tu novio, interrumpía.

– Olvídalo, ya pasó ¿Cómo has estado? Te veo contento.

– Bueno, sí. Mucho que contar. Mira.

Levantó el brazo, sacando con lentitud su mano con prótesis del abrigo,
la puso sobre la mesa. Separó la parte de su brazo y mostró el muñón. La
mujer levantó la cabeza, palideció.

– ¿Cuándo fue? ¿Cómo?

– Hace dos años, trabajando. Un desperfecto en los motores de la fábrica
echó a andar las aspas mientras hacía la mantención. Fue algo de
segundos, vi la sangre, el dolor, me desmayé. Dos días después desperté
en el hospital, sin mi mano. Desde entonces es que mi vida dio un giro
radical. Todo ha cambiado, no pude trabajar por un año por el trauma y
hasta ahora he realizado algunas cosas livianas. Eso no es todo.

– ¿Te indemnizaron?

– Una miseria, la empresa apenas se hizo cargo.

– ¡Que pena, de verdad! Recuerdo lo mucho que te gustaba hacer ejercicio
físico.

– Ahora lo hago, solo me falta la mano pero no el espíritu.

– ¿Y tienes novia o alguna amiga?

– No. Sabes que me cuesta eso. Te quería contar algo, otra cosa.

– A ver, ¿dime?

– Postulé a trabajar en las colonias, en Marte.

– ¿Quedaste?

– Sí, quedé. Quería contarte que tomo el transbordador en dos semanas.

– ¡Dios mío! Creía que solo admitían personas en buen estado físico… y
tu mano.

– Jugué mis cartas

– ¿Cómo, no entiendo?

– Hace unos diez años, poco antes de conocerte, llevaba un tiempo
trabajando en una compañía de comunicaciones.

En esas fechas las condiciones de trabajo eran duras. EL descontento se
hizo general y se paralizaron las operaciones. Fui elegido para
representar las demandas. Fueron varios días de negociaciones, el día
anterior de que todo volviera a la normalidad me dirigí a hablar con el
gerente general. Me llamó a mí teléfono personal muy temprano, requería
mi presencia.

– ¡Bienvenido! – me dijo.

– ¡Aquí estoy!

– ¿Qué le ha parecido todo?

– Es justo lo que pedimos

– Si veo, pero la empresa necesita continuar sus operaciones.

– Lo sabemos

– Tengo una oferta, tal vez sea ventajosa para usted.

En ese momento me estremecí, me ofrecía pagar los estudios de mi
hermana, además un año de sueldo sin trabajar y, por último, la deuda de
un favor por el buen servicio. Podría cobrarlo cuando quisiera.

– En ese entonces te conocí.

– Coincidimos en vacaciones, fue lindo conocerte.

– ¿Cómo te sientes?

– Era joven y fue un error, lo sé, no debí hacerlo.

– Ahí trabajaba mi padre, lo despidieron.

– No sabía, lo siento.

– Te tenía por una mejor persona.

– No ha sucedido de nuevo

– Sí, entonces ¿te vas, gracias a ese favor adeudado?

– Para siempre, logré mi lugar en las colonias, me espera un buen
trabajo y una mujer.

– Seguro serás feliz

– No lo sé, pero estaré bien

– Entonces, ¿por qué me pediste venir?

– Pensé que sería bueno contarte, verte la última vez. Todo el tiempo,
cada momento que he estado contigo lo he disfrutado.

– Yo también, ha sido una linda amistad

– Eso quería decirte

– ¿Eso?

– Me gustas, te quiero mucho.

Levantó la mirada, el café ya había acabado y solo quedaban los dos esa
noche en el local.

– Quisiera quedarme contigo esta noche.

– ¡No, no!

– ¿Por qué?

– ¡No quiero, no quiero. Simplemente vete!

El hombre se retiró lentamente, tomó su abrigo, caminó y desde la puerta
se dio la vuelta.

– ¡Adiós!

Sonó la campanilla de la puerta que cerraba, hubo un silencio profundo
en que la mujer apenas contenía las lágrimas. Exhaló solo una palabra de
despedida, que apenas sería escuchada.

– ¡Estúpido!
