#+TITLE: Ajuste de cuentas

-- ¿Me perdonas, pues?

Corría una brisa agradable a la orilla del rio, las hojas de los árboles resonaban
como suaves castañuelas a la espera de los tambores para iniciar la fiesta. Ese
casi silencio lo interrumpía un "tururu tururu" cada tantos segundos que se
paseaba de aquí para allá entre las ramas. Las dos mujeres sentadas a la
orilla, con los pantalones levantados y descalzas, los pies sumergidos en el
agua fresca, apenas balbuceandose palabras.

Juliana levantó la mirada con el ceño entrecerrado, le daban los rayos de sol de
pleno en el rostro.

-- Esperame un poquito --. Moviendo su trasero, se movió unos un par de cuartas para el lado hasta
   que logró llegar a la sombra.

-- ¿Y entonces, que harás?

-- Antonia, querida, si sabes nunca jamás podría estar enojada contigo mas de un
   ratico.

-- ¿Nada de rencor?

-- Bueno, solo un día o algo así. Además, ¿cúantos años sin vernos?

-- Años, y años pasaron en que soñé que me odiabas. Nuestras promesas, ahí en el olvido.

-- Mucha agua ha pasado bajo el puente.

-- Núnca pense que volvería a mi tierra.

-- Y aquí estás, sentada a mi lado.

-- Como cuando niñas, en el mismo rincón donde hacíamos nuestras trenzas.

-- Hasta cuando te fuiste con el Luciano.

-- Me prometió las estrellas. ¿Qué podía hacer? Era una niña casi.

-- Si, y me dejaste en este pueblo sola. ¿Valió la pena? ¿Que te entregó?

Antonia hizo el gesto de evitar la pregunta. Miró a la izquierda, le dió el sol,
miró abajo al agua y se distrajo largos segundos mirando una brisna de alga
moverse al son de la corriente. Le dolía recordar, le dolía sacar a flote la
verdad, aunque sea pesada, pasada debería estar.

-- Casi. Tuvimos un hijo, pero llegó la peste y ambos murieron, no pude de mi
   dolor, pasé pellejerías, trabajos de mierda. Y ¿qué de tí, saliste alguna vez del pueblo?

Juliana se acerca y la abraza, entre las suaves sombras de la orilla se aprecia
solo una forma de una multitud humana de dos y le empieza a contar casi en susurros.

-- Pues sí, me casé con un muchacho ferroviario y nos fuimos a vivir al norte.
Es bien diferente allá y nunca logré acostumbrame. Tuve dos niños que ya
hicieron su vida y volví al pueblo, a enterrar a mi madre. Pedro murió hace
pocos años ya.

-- Te ves bien. ¿Te quedarás acá?

-- Puede ser, si te quedas. Nada tengo que hacer por allá.

-- Tu madre estaría feliz.

-- Lo mismo creo.
