#+TITLE: Hueso de pollo
#+AUTHOR: David Pineda Osorio

Apenas había dejado de llover, el viento calaba los huesos de las piernas que
vestían un jean negro y delgado, sus zapatos mojados.  Llevaba esperando casi
una hora en esa esquina a que pasara la liebre hacia Puente Alto.

No se sentía bien, aunque logró hacer todo lo que debía por la mañana: una
entrevista de trabajo, una visita a la biblioteca en búsca de un viejo artículo
sobre los padres de la computación en el país. Había mascado una manzana de rojo
oscuro que terminó estando harinosa, lo peor y la dejó de lado. No tenía dinero para pasar
siquiera por un completo.

Le daba vueltas en la cabeza la idea de cambiar algunos detalles  en su
currículo. A unos quinientos metros se apareció el transporte con sus focos
encendidos, apenas alumbrando en las penumbras del día nublado. Llegaría a casa
a comer, se sentía hambriento, solo quería tirar la toalla y dormir todo el día.

-- Un pasaje, por favor -- entregó un billete de mil pesos, pagó al subir.

Recibió su boleto y una moneda de cien por vuelto, buscó un asiento y al fondo
logró ubicar uno desocupado. Pasó por el pasillo estrecho pidiendo permiso y se
instaló atrás de la baranda que da a la puerta.

Vió pasar las poblaciones de San Bernardo, el cruce de los Morros, la animita de
la difunta Correa abultada con montañas de botellas llenas de agua, campo a
diestra y siniestra, la planta faenadora de pollos y finalmente el paseo de las
Higueras en la villa de las parcelas, donde bajaría. Tuvo suerte de no quedarse
dormitando esta vez, entre las rendijas de la puerta pasaba un viento que lo
mantuvo despierto, tiritando casi. 

Tocó el timbre, en segundos el vehículo fue frenando para arrimarse a la
esquina de El Mariscal con Los Duraznos. Poco antes de detenerse saltó y
aterrizó con la precisión que da una práctica de años, partió corriendo en silencio, no
sabía si era por el ansia de llegar a casa o la calma de estar en un lugar seguro en que
podría descanzar y comer tranquilo, abrigado, sin ninguna persona extraña que le mirase.

Los bordes de la calzada estaban repletos de pastos y arbustos creciendo
pletóricos, los arreglos de ciruelos, las matas de agave en fila, unos cuantos
aromos, un par de palmeras, acacios, manchones de ortigas, dientes de león,
manzanillas, corazoncillos, relojillos y otras hierbas.

Una calle llena de árboles, portones abiertos y cerrados, paso a paso avanzando
rápido hasta el candado ya jubilado pero aún en uso que le permitiría entrar.
Salieron las dos perras de la casa a saludar, Kira que abrio la puerta de la
casa su compañera Amaya, una akita de hermoso pelaje negro, los otros perros debían de
estar dormitando por ahí o comiendo cruzando al patio de la cocina.

Le quedaron sus manos todas lengueteadas, al entrar tiró la mascarilla al tarro,
le dio un beso a su madre que estaba en la mesa sentada tejiendo junto a la
estufa y partio a  lavarse las manos. La madre, doña Marta, pone a calentar el
agua para servirle  un té a su hijo Vicente, saca el pan a la mesa. 

En las paredes estaban colgados algunos cuadros con fotografías de los hijos de
la familia en el colegio, la mayoría de los días de titulación, un par con los
niños más chicos juntos jugando, un recuadro de Jesús de Hoffmann manchado por
un pelotazo de hace años.

En la mesa habían unos pocillos con mermelada y mantequilla. Doña Marta 
preparaba un par de huevos en la paila, con la clara hasta el blanco y la yema
burbujeante. Conocía a su hijo y sabía que estaría feliz de recibirle algo de
amor hecho comida. Como siempre, no le molestaba que también 
se le acurrucaran alrededor también los  niños vecinos, las sobrinas, las
vecinas a quienes les atendía los pies para sus atenciones podológicas; algo
natural, sabía escuchar y compartir su sabiduría común.

-- Ma, hace mucho frio afuera, tenía mucha hambre. ¿Has estado bien? -- dijo
Vicente, tomó con una cuchara sopera el huevo frito mientras con la otra mano
hacía cabriolas para abrir la marraqueta y verter el contenido en esta.

-- Hijo, sí, bueno casi. ¿Acaso no ves?

-- ¿Qué? ¿Me llegó algún libro? ¿Y mi pá, dónde está?

-- No, no te ha llegado nada. Tu papá está al otro lado, en el patio. Piensa bien, mira a tu
alrededor.

-- Está silencioso. ¿Por qué no pones la radio mami? 

-- No hay ganas pues, ¿No te das cuenta, pequeño salvaje?

-- ¡Ay, mamá! ¿Qué pasa? ¿Será la gata Carlota?

-- ¡No!

-- ¿El Gigio? ¿Se murió el Gigio?

-- Se fue en el sueño.

-- ¡Ay, que pena más grande!

-- Escuchame, te voy a contar como fue que lo encontramos.

Por la puerta se asoma un hombre grandote, con una bufanda en torno al cuello,
calzado con bototos y un buzo que también usaba como pijama. Era el padre y le
gustaba ser la voz de la familia cuando se sentaba en la mesa. Saluda al hijo
poniendo la mano en su cabeza con un gesto cariñoso.

-- Mejor le cuento yo. -- dijo el padre, mientras ponía en la mesa una bolsa
llena de vainas de habas y dos bandejas, una con la que llenar con el grano y
otra con los desechos de las vainas. -- Mientras ayúdame a pelar estas habitas.

Luego de que te fuiste en la mañana salimos con tu madre de compras a la feria,
trajimos cargado el carrito con manzanas, naranjas, plátanos y verduras,
lechugas, acelgas unas cuantas alcachofas y las habas. Al viejo perro lo dejamos
durmiendo de lo más bien en el sillón,  ahí estaba roncando de lo más bien,
soñando sus sueños de perro. 

Le preguntamos a tu hermana, pero no nos quiso acompañar la Maritza porque dijo
que tenía que hacer unas tareas de la universidad. Se quedó escuchando música en
su pieza y ya tu sabes como es.

Cuando llegamos de vuelta a casa ahí estaba, a tu mamá que estaba sosteniendo el
carrito se le cayó de la sorpresa, había estirado la pata en el sueño. "Se va a
morir de pena la Mari".

Tu sabes, era chiquitita ella cuando trajimos al Gigio y crecieron juntos. Entonces
recogimos la fruta caída, entramos y fuimos directo a buscar una toalla o una
manta para cubrir el cuerpo. No pudimos ocultar nuestra llegada, pasaron unos
segundos cuando apareció tu hermana  y lo vió, gritó, gritó fuerte y lloró, se
arrodillo frente al perro muerto y le besó la cabeza, le tocó sus orejas medio
mordisqueadas, y lloró más. No pudimos taparlo hasta un rato despúes, la Martita
después de un rato tomó a la niña y se la llevó a su pieza, le dió un calmante y
se quedó dormida. 

El hombre seguía tomando las vainas, abriéndolas y separando lo bueno de
lo malo. El hijo y su madre le imitaban.

-- Entonces, de verdad se fue ya. Vivió muchos años -- dijo Vicente.

-- Eso que era un quiltro callejero que ni te digo, cuando salíamos en el auto
se nos escapaba y salía por el barrio a molestar a los otros perros -- recordó
Marta.

-- Y creo que habrá sobrevivido unos cuantos atropellos. Me acuerdo cuando
desapareció casi una semana y volvió cojeando. Le hicimos un rincón para que se
recuperara.

-- ¡Y papá! Le cocinaste sopa de huesos como por un mes y le dabas la mitad de
la olla cada vez.

-- Tenía que recuperarse pues, haría lo mismo por tí y ustedes por mí, es... era
parte de la familia.

Luego apareció Maritza, en la puerta con su polerón de capucha favorito,
verde claro y pantalón de tela, con pantuflas. "¿Dónde lo enterramos?" Preguntó como una
sombra.

-- Iré a buscar una pala, haremos un hoyo en el patio.

-- Está en la bodega hijo, toma la llave.

Salen al patio los dos hermanos, entran en la bodega, la puerta estaba abierta.
Dormía Baboo ahí acurrucado sobre un cartón, se levanta y deja el paso.

-- Lo siento mucho Mari -- dijo Vicente mientras tomaba una de las herramientas.

-- Era un hermano más -- escogío un chuzo, una muchacha fuerte también debe
saber manejar la barra se dijo a sí misma.

Salieron al patio y buscaron un lugar, ¿cuál podría ser? Debería ser el lugar
favorito en el que Gigio les hacía compañía.

-- Para mí también, estoy que lloro. Entonces, ¿te acuerdas?

-- Desde chiquita que me acuerdo, desde ese día que le dí mi pollo.

-- La mamá te había dicho que no pues. -- apuntando -- abajo de la higuera puede ser.

-- Buen lugar -- asintiendo. Nunca le hago caso a la mamá, tu sabes. 

-- Al final si, terminas haciendole caso.

-- Al principio no y eso es lo que vale. Préstame la barra.

Enterró el chuzo y removió la tierra, Maritza tomó la pala, sacaba la tierra y
piedras, lo que amontonaba a un costado mientras hacía más grande la cabidad,
suficiente para enterrar con seguridad a la criatura.

-- Entonces, el Gigio era un cachorrito y se tragó la presa que le diste. Al
rato comenzó  a toser y toser, estaba atragantado por un hueso. No tenías idea
que hacer,  no estaban los papás. -- Recordó Vicente mientras clavaba.

-- Y tu llamaste al primo Victor.

-- Si y me dio las instrucciones para salvarlo, había que sacarle el trozo de
hueso de la garganta.

-- Le metiste tu mano por la boca.

-- Y no solo la mano ¡che! El brazo hasta el codo, ni te acuerdas. -- hasta aquí,
señalando poco más arriba del codo.

Volvía a llover, cayéndo finas gotas entre las ramas de la higuera.

-- Le diste veinte años más de vida, gracias hermanito.

-- Lo recordaremos, está escrito en piedra.
